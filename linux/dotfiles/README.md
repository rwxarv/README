---
Title: Creating a Dotfiles Configuration Project Repo
Subtitle: Using GitLab to Backup and Share Your Config
tpl-h1duck: true
---

Create a `dotfiles` project repository on a [Git hosting service](https://duck.com/lite?kae=t&q=Git hosting service) like [GitLab](/service/gitlab/). Then add directories for each of the tools and services you will use.

:::co-stop
Don't forget to [configure Git](/tools/git/config/) first.
:::

* [Set Up Vim Dotfiles](./vim/)
* [Set Up TMUX Dotfiles](./tmux/)
