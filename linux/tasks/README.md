---
Title: Linux Operating System Tasks
tpl-h1duck: true
---

* Print Current Working Directory
* List Content of Directory
    * List Content of Current Directory
* Change Into a Directory
    * Change Into a Subdirectory
    * Change Into the Parent Directory
    * Change or Toggle To the Last Directory
* Make a Directory
* Edit a Text File with Vi/m
* Set Bash History to Vi Mode
* Use Tab Completion
* Rename a File

* Read a Manual Page
