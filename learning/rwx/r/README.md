---
Title: R is for Reading and Research
---

Even though as recently as the 1800s reading was something common folk did not have the privilege of learning today's world demands it for survival. If you can't read, you die, sometimes literally. Books have been the most comprehensive source of knowledge since before the printing press was invented and continue to be so. Learning from a book still outweighs the alternatives for comprehensive knowledge.

--------------------- ----------------------
Advantages            Disadvantages
--------------------- ----------------------
Comprehensively       Might miss important
address a topic.      things giving wrong
                      impression that
                      everything covered.

Single writer's       Only a single voice,
voice and style.      might lack concensus.

Well organized.       Humans don't think
                      linearly.     

Higher quality due    Difficult to keep up
to editing and        to date and publish.
publishing process.   

Portable despite      Difficult to search
sometimes still       quickly.
being printed
on paper.
--------------------- ----------------------

Books fill gaps. You might enjoy learning through videos, projects, and tutorials but until you work through something comprehensively you'll have gaps. For example, you might have installed and played around with eight different Linux distributions but if you still don't know how to redirect standard output to a file you have a gap.

Brains love stories. The mnemonics we read or create help us remember things. Competitive memorizers have been imagining creative stories that go with complicated number sequences and names for years. Something about narratives full of imagery, sound, and smell causing things to remain in our minds longer.

### Discussion Questions

* What are the not-so-obvious reasons to read?
* Describe how reading to learn is different than other reading?
* How are reading and researching related?
* What should you read? What should you not read?
