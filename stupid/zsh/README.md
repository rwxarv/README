---
Title: Don't Ever Use Zsh
Subtitle: Seriously, It will Destroy You
tpl-h1duck: true
---

Zsh is now the official shell of Apple. It's not the default on anything else. The *true* reason Apple picked it is because they are a heartless greedy company that never gives *anything* back to the [open source](/terms/open/source) community but *loves* to take and take and take claiming all the credit for itself. The idea that a license might *require* them to do the right thing terrifies them --- again, because Apple simply has no soul at all.

Bash continues to be the official default shell of all Linux systems. Ask yourself, do you want to learn some broken new default shell from a company that removed the `Escape` key and just wants to avoid GPLv3 or do you want to learn and use  Linux shell available on millions of systems *by default*? It's as simple as that. 

But if you are still here and care about some objective and *selfish* reasons *not to ever look at zsh* then keep reading.

Zsh provides no additional value over Bash, period. That is the biggest reason of all. The burden of proof is on Zsh to prove it is worth throwing out the default shell on *all* Linux systems for over two decades.

## No Exported Functions

Zsh handicaps you by preventing you from using critical Bash functionality such as exported functions (which is how all Bash completion on every Linux system is created in `/etc/bash_completion.d`). 

## Dangerous Floating Point Support

Zsh tempts you with floating point math in `$((3/2))` arithmetic variable expansion --- which seems great at first --- until you realize that people will attempt to use scripts code on other Linux and Unix systems where `/bin/sh` is *not* Zsh and you scripts will *horribly and disastrously fail*. The safest way to use floating point *has always been and continues to be the `bc` command*. Always assume *any* shell script is not floating point safe.

## Change Directories Dangerously Instead of `$CDPATH`

Zsh users tout the `cd` functionality without even knowing that `$CDPATH` exists in Bash to achieve the same result. And yes it can be used perfectly safely when used correctly, just like `PATH` can.

## Don't Attempt Redirection to Output a File

So, the following is just wrong.

```sh
# WRONG!
< somefile
```

This does not work on Bash. It might on Zsh (or some other irrelevant shell) but it doesn't in Bash. Telling people to do this is irresponsible without telling that it just won't work on most shells including the default.

Use `cat` instead.

```sh
cat somefile
```

## Don't `cat` Into a Pipe

This is just redundant and wasteful.

```sh
# WRONG!
cat foo | somecmd something`
```

This creates a subprocess unnecessarily.

Use redirection instead.

```sh
somecmd something < foo
```

## Failure to Meet POSIX Compliance

Zsh is *not* POSIX compliant as people claim. Use of that broken redirection suggestion is proof of that. The floating point math is another. If you are going to learn non-POSIX things they should be for the *default* Linux shell, not some broken upstart that can't decide what it wants to be. If you want POSIX use `/bin/dash` instead, which is what `/bin/sh` is linked to on all Debian-based systems.

