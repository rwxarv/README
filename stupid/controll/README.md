---
Title: Don't Use `Control-l` to Clear the Terminal Screen
Subtitle: Use `set -o vi` and `alias c=clear` If You Must
tpl-h1duck: true
---

Nothing shows how little you know about the Unix terminal than to recommend using `Control-l` or `^L` to clear the screen without understanding that `set -o vi` exists and what it does.

`Control-l` only works in Emacs mode (`set -o emacs`), which is the default, but you should seriously consider changing to `set -o vi` in order to leverage all your other [Vim](/tools/editors/vi/) muscle-memory. This is only the default because the GNU project created Bash and the GNU project invented and uses the horribly bloated and isolated Emacs editor.

If you really are that bothered by typing `clear` then add an `alias c=clear` to your Bash config. It's fewer keystrokes.
