---
Title: Linux Beginner Boost
---

* [Prerequisites](./prereqs/)
* [Annual Calendar](./calendar/)
* [Session Time Breakdown](./times/)
* [Target Occupations](./occupations/)
* [Frequently Asked Questions](./faq/)
* [What Comes Next](./whatsnext/)
* [Stats and Numbers](./stats/)

