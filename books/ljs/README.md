---
Title: Learning JavaScript (O'Reilly)
Subtitle: Good But Not Free
tpl-mdsearch: true
---

1. It costs money.
1. It can't be changed.
1. It doesn't cover HTTP.

*Learning JavaScript* from O'Reilly is one of the best books by far for learning *modern* JavaScript. It is, however, not free nor published under Creative Commons licensing as is [Eloquent JavaScript](/books/eloqjs/) and therefore is not the standard recommendation for this course.

![Front Cover of Learning JavaScript](./front.jpg)
