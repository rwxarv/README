---
Title: RWX.GG Progressive Knowledge App
tpl-h1off: true
tpl-style: style.css
---

## Hello Friend{.bigger .nolink}

[RWX.GG]{.spy} is an [open knowledge](/knowledge/) project and [progressive knowledge app](/knowledge/apps/) that seeks to do what it can to [fix](/learning/rwx/) education.

## Linux Beginner Boost{.bigger .nolink} 

A [flipped](/education/flipped/) and [intense](/boost/schedule/) [open course](/terms/open/course/) and community awaits. Chances are you [haven't](/boost/notothers/) experienced anything like it. You'll need more than just this app to get you through it. But don't worry. We got you.

## What Will You Learn?{.bigger .nolink}

First, how to become an effective, tech-focused, [autodidact](/learning/autodidact/). Then you'll learn the Linux terminal while you write *real* code in [nine different languages](/lang/). If you put in the time you'll not only be able to code remotely from any command line in the world, but you'll [grok](/terms/grok/) computers and *all* languages allowing you to learn any of them later.

## What Will You Get?{.bigger .nolink}  

A portfolio that will prove your skills and knowledge to everyone and anyone including yourself. Your [impostor syndrome](/learning/cognitive/impostor/) will be sent packing and your [Dunning-Kruger](/learning/cognitive/dk/) will be obliterated by authenticity. What comes next is up to you. What will your verse be?

## How Much?{.bigger .nolink}  

Nothing. This app and course will always be [free](/copyright/). This project is made possible through generous [donations](/contrib/#donating) and volunteer [contributions](/contrib/) from people like you.

[Rated PG-1337]{.bigger .center}

