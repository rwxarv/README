---
Title: Corruption Destroying Education
Subtitle: Don't Just Get angry. Get busy.
tpl-h1duck: true
---

The news in 2019 and 2020 has been full of specific examples of [widespread corruption of educational organizations](https://duck.com/lite?kae=t&q=widespread corruption of educational organizations) and companies providing certification. It is our goal to not just complain about these organizations and companies but to *do* something about it by providing better alternatives recognized by the industry thereby undercutting their monopolies and cronyism.

## College Board Proctors Being Paid Off

Several stories of [College Board proctors being paid off](https://duck.com/lite?kae=t&q=College Board proctors being paid off) and vows to protect against cheating on tests. Some of the fallout has included [colleges dropping the requirement to take the SAT test](https://duck.com/lite?kae=t&q=colleges dropping the requirement to take the SAT test).

## Several Ivy League Schools Busted for Fraud

Several have now plead guilty to [fraud when applying to get into high-priced universities.](https://duck.com/lite?kae=t&q=fraud when applying to get into high-priced universities.) While other [legacy applicants get in just because they have the right parents](https://duck.com/lite?kae=t&q=legacy applicants get in just because they have the right parents). Thankfully the world is waking up to this stupidity.

## CompTIA Litigates *Against* Right to Repair

In 2020 CompTIA, which the government of the United States official depends on and supports for several certifications, openly litigated against engineers gainfully employed repairing their electronic devices to seek to make such [basic rights to repair electronics illegal and have them require CompTIA](https://duck.com/lite?kae=t&q=basic rights to repair electronics illegal and have them require CompTIA) paid, renewed certifications.

## Apple Seeks Early Education Monopoly and Control

On the outside Apple appears to be charitably helping the educational organizations of the world when it fact it is pushing its own corporate agenda. Apple really only cares about [getting kids hooked early on Apple products and skills](https://duck.com/lite?kae=t&q=getting kids hooked early on Apple products and skills). Otherwise it would clearly use alternatives widely acknowledged by the industry instead.

* [Apple only makes products that teach children the Swift language](https://duck.com/lite?kae=t&q=Apple only makes products that teach children the Swift language,) which is widely known to effectively be an Apple-only language. 

* [Apple would have early learners start out on IPads](https://duck.com/lite?kae=t&q=Apple would have early learners start out on IPads) and not even use a keyboard becoming dependent on touch interfaces.

* School boards happily use [Apple products because Apple seeks to dominate early education](https://duck.com/lite?kae=t&q=Apple products because Apple seeks to dominate early education) and always has. Once a district has given in to the subsidized "deals" Apple then controls them fully.


