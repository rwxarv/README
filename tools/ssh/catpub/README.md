---
Title: Show Your Public Secure Shell (SSH) Key with `cat`
Subtitle: Careful, *Not* Your Private Key
---

Here's now to quickly display the content of your [Secure Shell public key](/tools/ssh/keygen/).

```sh
cat ~/.ssh/*.pub
```

Or of you want to display a specific one use something like this.

```sh
cat ~/.ssh/id25519.pub
```

You may wish to make an `alias` for that, or if you plan on adding it regularly to other scripts and automation you might even turn that into a script.

```sh
#!/bin/sh
cat "$HOME/.ssh/id25519.pub"
```
