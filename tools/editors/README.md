---
Title: Standard Text Editors
Subtitle: Which Do We Learn and Why
tpl-h1duck: true
---

A *text editor* is a technologist's most important tool, sort of like a sword is to a knight, a hammer to a carpenter. You get the point. There are three general types of text editors every technologist uses at some point. Here are the recommended editors based on the type.

--------------------------------------- -----------------------------------
 [Graphical](/terms/hci/ui/graphic/)       [VSCode](/tools/editors/vscode/)
 [Terminal](/terms/hci/ui/terminal/        [Vi/m](/tools/editors/vi/)
 [Command](/terms/hci/ui/command/          [Ed](/tools/editors/ed/)
--------------------------------------- -----------------------------------

The reason these are recommended above others is their widespread adoption and likelihood that the rest of your team will be using the same tools in a future occupation. In the case of [Vi](/tools/editors/vi/), the primary reason it is picked over something like [Emacs](/tools/editors/emacs/) (which is a fine editor) is simply because Emacs must be installed and Vi does not.

:::co-rage
Fighting over one's favorite editor is one of the most common historical pointless battles in the industry. Pick the right tool for the job. 
:::

:::co-fyi
An [IDE](https://duck.com/lite?kae=t&q=IDE) will certainly come with a text editor built into it, but by definition is not *just* a text editor since it does a lot more than just edit text.
:::
