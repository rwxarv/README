---
Title: TMUX Tasks
tpl-h1duck: true
---

* List All Sessions
* Attach and Resume Last Session
* Detach from Running Session
