---
Title: Bash Mnemonic Mini-Projects
Subtitle: Exercise Those Skills. 
---

Here's a beginning list of project ideas for practicing your Bash scripting skills.

### Beginner

* [Greet](./greet/)
* [Nyan](./nyan/)
* [Waffles](./waffles/)
* Badgers
* EightBall
* [Colors](./colors/)
* BridgeKeeper
* DiceRoller
* BaseCounter
* Enigma
* QuizGame
* StoryGame

### Intermediate

* PandocSSG
* TwitchTool (`curl`, web API, GraphQL)
* RepoTool
* PDFViewer
* SpotifyTool
* `.bashrc`
