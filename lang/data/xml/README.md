---
Title: eXtendible Markup Language / XML
Subtitle: World's Most Hated Structured Data Language
tpl-h1duck: true
---

```xml
<?xml version="1.0" encoding="UTF-8"?>
<record>
  <pie>Apple</pie>
  <numbers>
    <val>10</val>
    <val>3.141592653589</val>
    <val>6.673e-11</val>
  </numbers>
  <likes val=true/>
  <nothing val=null/>
</record>
```

Although XML still has great value for specific applications it is widely abhorred among developers due to it's unnecessarily complex, bloated, verbose, and difficult to read syntax. 

However, many traditional digital document formats use it such as Word or LibreOffice as well as SVG images. 

For a time XHTML was actually thing, until it wasn't. HTML came from a language called SGML and XHTML came from XML. Both were designed by computer scientists who never intended people to actually use text editors to compose files in the language. The courageous designers of HTML5 decided to break from the past and declare it to be its own language, not derived from *anything* else any longer. Thank God they did.
