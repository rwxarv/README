---
Title: GitLab
Subtitle: World's *Best* Git Hosting Service
tpl-h1duck: true
---

GitLab is a [far superior](isbest/) alternative to [GitHub](../github/) mostly because of its independent and [open source](/terms/open/source/) nature and [DevOps](/terms/devops/) built in integration, but also because of its pure and elegant [UX](/terms/hci/ux/).

