---
Title: MC Paper Server Tasks
tpl-h1duck: true
---

* Download Server Jar File 
    * Find Paper Server Jar File on Web
    * Copy Latest Link Address
    * Download File with Curl
    * Check the Type of File
* Run Server Jar with Java to Create EULA
* Agree to the EULA
    * List the Current Files
    * Change to `true` in `eula.txt`
* Update Server Properties
    * Update Server Port
    * Activate Command Blocks
* Run Server Jar with Java
* Test Client Connection to Server

