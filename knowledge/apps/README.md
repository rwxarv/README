---
Title: Progressive Knowledge Apps
Subtitle: Better Than Books and Wikis
tpl-h1duck: true
---

A *progressive knowledge app* is a [minimum viable knowledge base](/knowledge/mvkb/) written entirely in [Markdown](/lang/md/) and rendered as a [progressive web app](/terms/web/pwa/) but can equally be rendering in any number of consumable media formats including ePub, Kindle, PDF, and others. 

## Localized Searching

Modern web browsers support [web service workers](https://duck.com/lite?kae=t&q=web service workers) allowing PKAs to contain their own localized search engines built upon meta data derived from the structured knowledge content without need for any centralized search engine or even an Internet connection once the content has been synced.

PKAs automatically include `README.md` files in addition to the rendered `index.html` files allowing command line tools to be easily created or even just raw [`curl`](https://duck.com/lite?kae=t&q=`curl`) downloads that anyone can render as they like with [Pandoc](/tools/pandoc/).

All PKAs are also maintained as public [Git](/tools/git/) project repos.

## Guided External Searching

Half the trouble of learning on one's own is knowing what to look for. Knowledge apps are in a unique position to craft  queries and redirect those learning to results from *reliable* search engines. Those learning simply click on [links like this one to anything](https://duck.com/lite?kae=t&q=links like this one to anything) which look like this in [Markdown](/lang/md/):

```md
simply click on [links like this one to anything](https://duck.com/lite?kae=t&q=links like this one to anything) which look like this
```

These are translated into searches for the highlighted words like this:

```md
simply click on [links like this one to anything](https://duck.com/lite?kae=t&q=links like this one to anything) which look like this
```

This creates a unique guided Web browsing experience that is unlike anything possible in other media formats.

## Social Media App?

Progressive knowledge apps are fundamentally social media apps as well, but not in the traditional sense. They are social in that they depend on and refer to an active community dedicated to helping one another. A PKA is as dependent on *connections through community* as it is on *content*. In fact, the community *maintains* the content in a unique way different from [wikis](/terms/wikis/) by preserving a single, clear voice and target reader.

In addition to the community PKA members are encouraged to create their own README knowledge bases --- which they too can build into their own PKAs --- and to subscribe to one another directly or with the help of the [README.World exchange](https://readme.world).

## Promoting Personal Research with External Links

The Web has developed an anti-pattern that dictates one should *reduce* the number of outbound links in order to promote a higher [SEO](https://duck.com/lite?kae=t&q=SEO) score and therefore more money. This has had a disastrous affect on the quality of all knowledge on the Web but --- more importantly --- it has trained way too many people to *not* do any additional research. 

Progressive knowledge apps instead *promote* active outbound linking to other content to encourage the learner to research as many positions and opinions as possible in order to *counteract* cognitive biases. 

In fact, PKA renderers commonly render empty keyword and concept links as [DuckDuckGo](https://duck.com/lite?kae=t&q=DuckDuckGo) searches until localized explanations can replace them.

:::co-rage

## Battling the Zombie Makers

Facebook's actions demonstrate that it never wants anyone to leave their service. They are financially incented to keep their users spending time in their own personalized ideological echo chambers promoting as much confirmation bias and false dopamine highs as possible. Evidence suggests they have created task forces specifically to promote active addiction --- even in children --- to their "service." Progressive knowledge apps are but a small effort to counter the 2.6 billion users Facebook regularly renders educationally catatonic.

:::
