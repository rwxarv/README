---
Title: Knowledge as Source
Subtitle: Capturing, Maintaining, and Sharing Knowledge as Source Code
tpl-h1duck: true
---

We live in a time when the amount of human knowledge has reached unprecedented levels, but we are at risk. So much knowledge is being produced in different forms that strategies for maintaining and archiving it in a way that is not owned by proprietary interests have been all but ignored. 

By applying the best practices and lessons learned from managing and distributing the source code of [opensource](/terms/open/source/) software to *knowledge* source we can leverage the best practices and approaches that have made these projects so successful. This begins by capturing the knowledge in a [basic form](/lang/md/basic/) that can be used easily with source management and version control tools [such as Git](./git/) and then building [knowledge applications](/knowledge/apps/) from the that source.
