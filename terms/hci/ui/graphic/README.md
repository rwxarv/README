---
Title: What is a Graphical User Interface / GUI?
Subtitle: A Graphical Way for You to Use Something
tpl-h1duck: true
---

A *graphical user interface* or *GUI* is an [interface](/terms/interface/) that allows you to control something, usually a computer or device. GUIs have become standard for most all computers but [servers](/terms/server/) frequently do not have one requiring proficiency with a [terminal user interface](https://duck.com/lite?kae=t&q=terminal user interface) or [*command line interface*](../command).
