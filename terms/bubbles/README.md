---
Title: What's a Bubble?
Subtitle: Tech, Real Estate, Relationships
tpl-h1duck: true
---

Bubbles are simply things that become overvalued without people realizing it so that they continue to be blown up. Eventually they burst when people realize how outrageous valuations have become. The ["DotCom" collapse in 2000](https://duck.com/lite?q="DotCom" collapse in 2000) is the most famous tech bubble on record.

## See Also

* [Russ Explains Priorities That Cause Bubbles](https://youtu.be/BzAdXyPYKQo) (swearing)
