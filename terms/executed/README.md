---
Title: What does Executed Mean?
Subtitle: A Command or Program That's Been Run
---

To *execute* means *to carry out* and in the tech world that usually means that a command or program has done the bidding of its master user.
