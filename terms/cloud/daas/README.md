---
Title: What is Database as a Service / DaaS?
Subtitle: Providing Database Services in the Cloud
tpl-h1duck: true
---

*Database as a service* allows queries that would normally require a database installation (and a host server to run it on) to be done securely over the Internet. [FaunaDB](https://fauna.com) is one example of DaaS.

## See Also

* [Cloud Services](/terms/cloud/)
