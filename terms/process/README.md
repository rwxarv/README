---
Title: What is a Computer Process?
Subtitle: A Running Program
tpl-h1duck: true
---

A *process* is simply a running program started either directly by a [user](/terms/users/) or indirectly by the system itself.

:::co-tip
Use the `top` command to see the running processes in real time.
:::
